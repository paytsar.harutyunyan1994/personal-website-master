window.addEventListener('load', drawSection);

function drawSection() {
    const section = document.querySelector('.app-section.app-section--image-culture');
    section.insertAdjacentHTML('afterend', createSection());
    const form = document.getElementById('form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();
        console.log(event.target.elements.email.value);
    });
}

function createSection() {
    const section = document.createElement('section');
    section.setAttribute('class', 'app-section__join--our-programapp');

    const h2 = document.createElement('h2');
    h2.setAttribute('class', 'app-title1');
    h2.innerHTML = 'Join Our Program';

    const h3 = document.createElement('h3');
    h3.setAttribute('class', 'app-subtitle1');
    h3.innerHTML = 'Sed do eiusmod tempor incididunt <br/> ut labore et dolore magna aliqua.';

    const form = document.createElement('form');
    form.setAttribute('id', 'form');
    form.setAttribute('class', 'app-section__checkbox-container');

    const input = document.createElement('input');
    input.setAttribute('class', 'form-input');
    input.setAttribute('type', 'email');
    input.setAttribute('id', 'email');
    input.setAttribute('placeholder', 'Email');
    input.setAttribute('name', 'email');

    const button = document.createElement('button');
    button.setAttribute('class', 'app-section__button--subscribe');
    button.setAttribute('type', 'submit');
    button.innerHTML = 'SUBSCRIBE';

    section.appendChild(h2);
    section.appendChild(h3);
    form.appendChild(input);
    form.appendChild(button);

    section.appendChild(form);
    appendStyles();

    return section.outerHTML;
}

function appendStyles() {
    const style = document.createElement('style');
    style.innerHTML = `
        .app-section__button--subscribe {
            border-radius: 26px;
            border: 0;
            font-size: 14px;
            height: 36px;
            letter-spacing: 1.2px;
            margin: 60px auto 108px;
            text-transform: uppercase;
            width: 111px;
            background: #55C2D8; 
            color:#fff;
            cursor: pointer;
        }
        
        .app-section__join--our-programapp {
            height: 436px;
            max-width: 100%;
            color: white;
        }
        
        .app-title1 {
            padding-top: 87px;
            font-family: 'Oswald';
            font-style: normal;
            font-weight: 700;
            font-size: 48px;
            line-height: 64px;
            text-align: center;
        }
        
        .app-subtitle1{
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            font-size: 24px;
            line-height: 32px;
            text-align: center;
            color: rgba(255, 255, 255, 0.7);
        }
        
        .app-section__checkbox-container{
            text-align: center;
        }
        
        .form-input {
            margin-right: 50px;
            height: 26px;
            width: 350px;
            background: rgba(255, 255, 255, 0.70);
        }
    `;
    document.head.appendChild(style);
}